# Building Browser Extensions with Laravel Mix #
Learn how to use Laravel Mix to easily build browser extensions.

### What is Laravel Mix?
An elegant wrapper around Webpack for the 80% use case.
https://laravel.com/docs/5.6/mix

### Advantages ###
* Easy to use build tool
* Easily use vuejs single file components (.vue)
* CSP-compliant browser extensions built with vuejs framework

### Requirements ###
* npm

### Setup ###
1. Install npm packages in package.json.
`npm install`
2. Build the extension using the following npm scripts:
    * Building for Chrome:
        * dev-chrome
        * watch-chrome
        * staging-chrome
        * prod-chrome
    * Building for Firefox:
        * dev-firefox
        * watch-firefox
        * staging-firefox
        * prod-firefox

    Ex: `npm run prod-chrome`
    This will build the extension for production environment and put it inside `dist/chrome/production` folder.